package cat.itb.kruger_project.ui.main;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import cat.itb.kruger_project.R;

public class MainFragment extends AppCompatActivity {

    public static final int REQUEST_CODE_PHOTO_ANIMAL = 112;

    ImageView imageView;
    StorageReference mStorageRef;
    public Uri imgUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_fragment);

        mStorageRef = FirebaseStorage.getInstance().getReference("Images");

        EditText name = findViewById(R.id.editnomanimal);
        Button saveBttn = findViewById(R.id.savebtn);

        imageView = findViewById(R.id.cameraicon);


        imageView.setOnClickListener(view -> selectPhoto());
        saveBttn.setOnClickListener(view -> saveAnimalName(name));

    }


    private void selectPhoto() {

        Intent selectPictureIntent = new Intent();
        selectPictureIntent.setType("image/*");
        selectPictureIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(selectPictureIntent, REQUEST_CODE_PHOTO_ANIMAL);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode== REQUEST_CODE_PHOTO_ANIMAL && resultCode==RESULT_OK && data!=null && data.getData() != null){

            imgUri =data.getData();
            imageView.setImageURI(imgUri);

        }
    }

    private void saveAnimalName(EditText editName) {

        EditText editAName = findViewById(R.id.editnomanimal);
        String animalName = editAName.getText().toString();

        Intent intent = new Intent();
        intent.putExtra("nameAnimal", animalName);
        setResult(Activity.RESULT_OK, intent);

        if (imgUri != null) {
            String imgname = System.currentTimeMillis() + "." + obtainUri(imgUri);

            intent.putExtra("nameImg", imgname);

            StorageReference Ref = mStorageRef.child(imgname);

            KrugerMap.storageReference = Ref;
            KrugerMap.imgURI = imgUri;

            Ref.putFile(imgUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            // Get a URL to the uploaded content

                            Toast.makeText(MainFragment.this, "Upload succesful", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            // Handle unsuccessful uploads

                            Toast.makeText(MainFragment.this, "Upload Failed", Toast.LENGTH_SHORT).show();
                        }
                    });
        }
        else
        {
            KrugerMap.imgURI = null;
            KrugerMap.storageReference = null;
        }
        finish();
    }

    public static String obtainAnimalString(Intent intent){
        return intent.getStringExtra("nameAnimal");
    }

    public static String obtainImgString(Intent intent){
        return intent.getStringExtra("nameImg");
    }

    public String obtainUri(Uri uri){

        ContentResolver cr = getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(cr.getType(uri));

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }


   /* public void changeToMap() {
        Intent mapIntent = new Intent(MainFragment.this, KrugerMap.class);
        // Verify that the intent will resolve to an activity
        if (mapIntent.resolveActivity(this.getPackageManager()) != null) {
            startActivity(mapIntent);
        }

    }*/

}
