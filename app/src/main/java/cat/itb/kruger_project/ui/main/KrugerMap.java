package cat.itb.kruger_project.ui.main;
import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import cat.itb.kruger_project.R;

public class KrugerMap extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener {

    private static final int REQUEST_LOCATION = 1;
    public static final int REQUEST_CODE_NOM_ANIMAL = 111;
    public GoogleMap mMap;
    private LocationManager locationManager;

    public ArrayList<ArrayList>markers = new ArrayList<>();

    public String nameAnimal = "ERROR";
    public String imgName;
    public static StorageReference storageReference;
    public static Uri imgURI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityCompat.requestPermissions(this,new String[]
                {Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);

        setContentView(R.layout.activity_kruger_map);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */

    @Override
    public void onMapReady(GoogleMap googleMap) {

        Button addbutton = findViewById(R.id.addbtn);

        mMap = googleMap;

        //mMap.setMinZoomPreference(9);               //Maxim lluny
        mMap.setMaxZoomPreference(16);              //Maxim Zoom


        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);

        LatLng Default = new LatLng(-23.987399, 31.556050);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(Default));

        addbutton.setOnClickListener(view -> addMarker(view, mMap));

    }

    private void addMarker(View view, GoogleMap map) {

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
        {
            activateGPS();
            getLocation();

        }


        backToMainFragment();

    }

    private void backToMainFragment() {

        Intent intent = new Intent(KrugerMap.this, cat.itb.kruger_project.ui.main.MainFragment.class);
        startActivityForResult(intent, REQUEST_CODE_NOM_ANIMAL);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        GoogleMap map = mMap;

        double [] lastLocation = getLocation();

        String iconBase = "https://maps.google.com/mapfiles/kml/shapes/";

        switch (requestCode) {
            case REQUEST_CODE_NOM_ANIMAL:
                if (resultCode == Activity.RESULT_OK) {
                    String nomAnimal = cat.itb.kruger_project.ui.main.MainFragment.obtainAnimalString(data);
                    String imgName = cat.itb.kruger_project.ui.main.MainFragment.obtainImgString(data);
                    Toast.makeText(this, "New animal added "+nomAnimal.toLowerCase(), Toast.LENGTH_SHORT).show();
                    this.nameAnimal = nomAnimal;
                    this.imgName = imgName;
                } else {
                    this.nameAnimal = "ERROR";
                }
        }

        if (!nameAnimal.equals("ERROR") && (!nameAnimal.equals("")) && (nameAnimal != null)){
            if (lastLocation != null)
            {
                double longitude = lastLocation[0];
                double latitude = lastLocation[1];

                LatLng newLocation = new LatLng(longitude, latitude);

                ArrayList<Object> detallsMarker = new ArrayList<>();

                MarkerOptions marker = new MarkerOptions().position(newLocation).title(nameAnimal).icon(changecolor(nameAnimal));

                map.addMarker(marker);
                detallsMarker.add(marker);
                detallsMarker.add(storageReference);
                detallsMarker.add(imgName);

                markers.add(detallsMarker);

                mMap.setOnInfoWindowClickListener(this::onInfoWindowClick);

                mMap.moveCamera(CameraUpdateFactory.newLatLng(newLocation));

                DetailMarker.storageReference = storageReference;

            }
        }

        this.mMap = map;

    }

    private BitmapDescriptor changecolor(String nameAnimal) {
        switch (nameAnimal){
            case "Lion":
                return BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE);

            case "Leopard":
                return BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE);

            case "Buffalo":
                return BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED);

            case "Elephant":
                return BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW);

            case "Rhino":
                return BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET);

            case "Cheetah":
                return BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN);

            case "Wolf":
                return BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE);

            default:
                return BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN);

        }

    }

    private double [] getLocation() {

        if (ActivityCompat.checkSelfPermission(KrugerMap.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(KrugerMap.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this,new String[]
                    {Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        }
        else{
            Location GPSLoc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            Location INTLoc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            Location PASSLoc = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);

            if (GPSLoc != null)
            {
                double lat = GPSLoc.getLatitude();
                double lng = GPSLoc.getLongitude();

                return new double[] {lat, lng};

            }
            else if (INTLoc != null)
            {
                double lat = INTLoc.getLatitude();
                double lng = INTLoc.getLongitude();

                return new double[] {lat, lng};
            }
            else if (PASSLoc != null)
            {
                double lat = PASSLoc.getLatitude();
                double lng = PASSLoc.getLongitude();

                return new double[] {lat, lng};
            }
            else
            {
                Toast.makeText(this, "Ubication not found", Toast.LENGTH_SHORT).show();
                return null;
            }
        }
        return null;
    }

    private void activateGPS() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage("Activate GPS").setCancelable(false).setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));

            }
        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();

            }
        });

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth()/5, vectorDrawable.getIntrinsicHeight()/5);
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth()/5, vectorDrawable.getIntrinsicHeight()/5, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        Intent intent = new Intent(KrugerMap.this, DetailMarker.class);
        intent.putExtra("nameAnimal", marker.getTitle());

        for (int i=0; i<markers.size(); i++){
            ArrayList <Object> aux = markers.get(i);
            if (marker != aux.get(0)){
                intent.putExtra("nameImg", aux.get(2).toString());
                DetailMarker.storageReference = (StorageReference) aux.get(1);
            }
        }

        startActivity(intent);
    }
}
