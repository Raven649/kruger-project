package cat.itb.kruger_project.ui.main;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import cat.itb.kruger_project.R;

public class DetailMarker extends AppCompatActivity {

    public String nameAnimal;
    public static StorageReference storageReference;
    public static Uri imgURI;
    public static String imgName;
    TextView detailsName;
    TextView detailsDescription;
    ImageView detailsPic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

       detailsName = findViewById(R.id.detallsnom);
       detailsDescription = findViewById(R.id.descripciodetail);
       detailsPic = findViewById(R.id.pictureDetail);

        Intent intent = getIntent();

        nameAnimal = intent.getExtras().getString("nameAnimal");
        imgName = intent.getExtras().getString("nameImg");

        if (storageReference != null){
            storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    Picasso.with(DetailMarker.this).load(uri).into(detailsPic);
                    detailsPic.setImageURI(uri);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(DetailMarker.this, "Unable to trace image", Toast.LENGTH_LONG).show();
                }
            });
        }



        detailsName.setText(nameAnimal);

        switch (nameAnimal){
            case "Lion":
                detailsDescription.setText("Just a Lion");
                break;
            case "Pard":
                detailsDescription.setText("Just a Leopard");
                break;
            case "Buffalo":
                detailsDescription.setText("Just a Buffalo");
                break;
            case "Elephant":
                detailsDescription.setText("Just an Elephant");
                break;
            case "Rhino":
                detailsDescription.setText("Just a Rhino");
                break;
            case "Cheetah":
                detailsDescription.setText("Just a Cheetah");
                break;
            case "Wolf":
                detailsDescription.setText("Just a Wolf");
                break;
            default:
                detailsDescription.setText("Without Details");
                break;
        }

    }
}
